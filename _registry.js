const fs = require("fs");

const ROOT_PATH = __dirname;
const JSON_PATH = `${ROOT_PATH}/icons.json`;

exports.ROOT_PATH = ROOT_PATH;
exports.JSON_PATH = JSON_PATH;

exports.load = function load() {
    return fs.existsSync(JSON_PATH)
        ? JSON.parse(fs.readFileSync(JSON_PATH).toString())
        : [];
}

exports.save = function save(icons) {
    const json_data = icons.sort((a, b) => {
        if (a.file < b.file) {
            return -1;
        } else if (a.file > b.file) {
            return 1;
        }

        return 0;
    });

    fs.writeFileSync(JSON_PATH, JSON.stringify(json_data, null, 4));
}
