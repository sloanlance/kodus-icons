/**
 * This script imports icons from third-party icon libraries to the JSON database.
 */

const dir = require("node-dir");
const path = require("path");
const fs = require("fs");
const md5_file = require('md5-file');
const registry = require("./_registry");

const ROOT_PATH = registry.ROOT_PATH;

let icons = [];     // the full catalog of icons (loaded/saved to the JSON registry)
let name_map = {};  // tracks unique names during import
let md5_map = {};   // tracks unique file content during import
let file_map = {};  // tracks unique files (relative paths) during import
let file_seen = {}; // tracks files seen during import (so we can remove file from the registry on save)

let dupes = 0;

load_json();
import_icons();
save_json();

if (dupes) {
    console.warn(`omitted ${dupes} duplicate files.`);
}

function load_json() {
    icons = registry.load();

    icons.forEach(icon => file_map[icon.file] = icon);
}

function import_icons() {
    import_material_icons();
    import_custom_icons();
}

function register_icon(name, category, path) {
    let file = path.substr(ROOT_PATH.length + 1);

    if (name_map[name]) {
        console.warn(`duplicate icon name detected: ${name}`);
    }

    const md5 = md5_file.sync(path);

    if (md5_map[md5]) {
        console.warn(`duplicate file detected: ${file} [${md5}]`);
        dupes++;
    } else {
        if (file_map[file]) {
            md5_map[md5] = file_map[file];
        } else {
            const icon = { name, category, file, include: true, keywords: "" };

            icons.push(icon);
            
            md5_map[md5] = icon;
            file_map[file] = icon;
        }

        name_map[name] = true;
        file_seen[file] = true;
    }
}

function import_material_icons() {
    const MATERIAL_ICON_CATEGORIES = [
        "action",
        "alert",
        "av",
        "communication",
        "content",
        "device",
        "editor",
        "file",
        "hardware",
        "image",
        "maps",
        "navigation",
        "notification",
        "places",
        "social",
        "toggle",
    ];

    const MATERIAL_PATTERN = /\/ic_(\w+)_24px.svg$/;

    MATERIAL_ICON_CATEGORIES.forEach(category => {
        const path = `${ROOT_PATH}/node_modules/material-design-icons/${category}/svg/production/`;

        const files = dir.files(path, { sync: true });

        for (let file of files) {
            let match = MATERIAL_PATTERN.exec(file);

            if (match) {
                const name = match[1].replace(/_/g, "-");

                register_icon(name, category, file);
            }
        }
    });
}

function import_custom_icons() {
    const path = `${ROOT_PATH}/custom_icons/`;

    const files = dir.files(path, { sync: true });

    const SVG_PATTERN = /\/(\w+).svg$/;

    for (let file of files) {
        let match = SVG_PATTERN.exec(file);

        if (match) {
            const name = match[1].replace(/_/g, "-");

            register_icon(name, "custom", file);
        }
    }
}

function save_json() {
    icons = icons.filter(icon => {
        if (file_seen[icon.file]) {
            return true;
        } else {
            console.warn(`file removed from database: ${icon.file}`);

            return false;
        }
    });

    registry.save(icons);

    console.log(`created: ${registry.JSON_PATH}`);
}
