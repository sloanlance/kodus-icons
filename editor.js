const express = require('express')
const body_parser = require('body-parser')
const app = express()
const port = 3000
const registry = require('./_registry')

app.use('/', express.static('.'))

app.use(body_parser.json({ limit: '1mb' }))

app.post('/icons.json', (req, res) => {
    let icons = req.body.icons

    registry.save(icons)

    res.sendStatus(204)
})

app.listen(port, () => {
    console.log(`Library editor listening on port ${port}!`)
})
