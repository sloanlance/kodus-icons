# kodus/icons

This package catalogs and builds the Kodus icon-library.

The icons are imported from various packages in the `icons.json` registry, which can be updated:

    npm run import

The registry can be edited from a simple graphical front-end by launching the editor:

    npm run editor

Then open e.g. `http://localhost:3000` in a browser.

## Naming Conventions

  * Icons are *preferably* named according to what they depict, rather than what they mean.

  * Symbols with a *generally unambiguous* meaning may be named according to what they mean,
    but the `keywords` property must indicate what they depict, if anything.

  * Variants should be named with a *suffix*, e.g. `cloud-circle`, `plus-circle-outline`, etc.
